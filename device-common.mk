#
# Copyright (C) 2016 ShevT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

DEVCOMMON_PATH := device/huawei/omap4-common

# Processor
TARGET_BOARD_OMAP_CPU := 4460

PRODUCT_CHARACTERISTICS := default

DEVICE_PACKAGE_OVERLAYS += $(LOCAL_PATH)/overlay/common

# Audio Packages
PRODUCT_PACKAGES += \
    tinycap \
    tinymix \
    tinyplay \
    libtinyalsa \
    libaudioutils

# Hardware HALs
PRODUCT_PACKAGES += \
    audio.a2dp.default \
    audio.usb.default \
    audio.r_submix.default \
    audio.primary.omap4 \
    gralloc.omap4.so \
    camera.omap4 \
    memtrack.omap4 \
    power.omap4 \
    lights.omap4

# tzdata
PRODUCT_COPY_FILES += \
    $(DEVCOMMON_PATH)/tzdata/tzdata:system/usr/share/zoneinfo/tzdata

# init.d
PRODUCT_COPY_FILES += \
    $(DEVCOMMON_PATH)/utilities/init.d/11frandom:system/etc/init.d/11frandom

# Media / Audio
PRODUCT_COPY_FILES += \
    frameworks/av/media/libstagefright/data/media_codecs_google_audio.xml:system/etc/media_codecs_google_audio.xml \
    frameworks/av/media/libstagefright/data/media_codecs_google_telephony.xml:system/etc/media_codecs_google_telephony.xml \
    $(DEVCOMMON_PATH)/configs/media/media_profiles.xml:system/etc/media_profiles.xml \
    $(DEVCOMMON_PATH)/configs/media/media_codecs.xml:system/etc/media_codecs.xml \
    $(DEVCOMMON_PATH)/configs/audio/audio_effects.conf:system/etc/audio_effects.conf \
    $(DEVCOMMON_PATH)/configs/audio/audio_policy.conf:system/etc/audio_policy.conf

# WiFi
PRODUCT_PACKAGES += \
    dhcpcd.conf \
    hostapd \
    hostapd_default.conf \
    libwpa_client \
    wpa_supplicant \
    wpa_supplicant.conf

# Common
PRODUCT_PACKAGES += \
    make_ext4fs \
    setup_fs \
    e2fsck \
    com.android.future.usb.accessory

# Utilities
PRODUCT_PACKAGES += \
    remount \
    optimizedb

# Post init script
PRODUCT_PACKAGES += \
    post-init.sh

# Supolicy
PRODUCT_PACKAGES += \
    supolicy \
    libsupol.so

# Network tools
PRODUCT_PACKAGES += \
    libpcap \
    tcpdump

# IPv6 tethering
PRODUCT_PACKAGES += \
    ebtables \
    ethertypes

# Charging mode
PRODUCT_PACKAGES += \
    charger_res_images

# Remove packages
PRODUCT_PACKAGES += \
    RemovePackages

# Key maps
PRODUCT_COPY_FILES += \
    $(DEVCOMMON_PATH)/configs/usr/omap4-keypad.kl:system/usr/keylayout/omap4-keypad.kl \
    $(DEVCOMMON_PATH)/configs/usr/omap4-keypad.kcm:system/usr/keychars/omap4-keypad.kcm \
    $(DEVCOMMON_PATH)/configs/usr/twl6030_pwrbutton.kl:system/usr/keylayout/twl6030_pwrbutton.kl

# Input device calibration files
PRODUCT_COPY_FILES += \
    $(DEVCOMMON_PATH)/configs/usr/syn_tm12xx_ts_1.idc:system/usr/idc/syn_tm12xx_ts_1.idc \
    $(DEVCOMMON_PATH)/configs/usr/syn_tm12xx_ts_2.idc:system/usr/idc/syn_tm12xx_ts_2.idc

# These are the hardware-specific features
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/handheld_core_hardware.xml:system/etc/permissions/handheld_core_hardware.xml \
    frameworks/native/data/etc/android.hardware.telephony.gsm.xml:system/etc/permissions/android.hardware.telephony.gsm.xml \
    frameworks/native/data/etc/android.hardware.bluetooth_le.xml:system/etc/permissions/android.hardware.bluetooth_le.xml \
    frameworks/native/data/etc/android.hardware.camera.flash-autofocus.xml:system/etc/permissions/android.hardware.camera.flash-autofocus.xml \
    frameworks/native/data/etc/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
    frameworks/native/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
    frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
    frameworks/native/data/etc/android.hardware.wifi.direct.xml:system/etc/permissions/android.hardware.wifi.direct.xml \
    frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:system/etc/permissions/android.hardware.sensor.accelerometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.proximity.xml:system/etc/permissions/android.hardware.sensor.proximity.xml \
    frameworks/native/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
    frameworks/native/data/etc/android.hardware.sensor.gyroscope.xml:system/etc/permissions/android.hardware.sensor.gyroscope.xml \
    frameworks/native/data/etc/android.hardware.sensor.barometer.xml:system/etc/permissions/android.hardware.sensor.barometer.xml \
    frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
    frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml \
    frameworks/native/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml \
    frameworks/native/data/etc/android.hardware.usb.host.xml:system/etc/permissions/android.hardware.usb.host.xml

ADDITIONAL_DEFAULT_PROPERTIES += \
    ro.allow.mock.location=1 \
    persist.sys.usb.config=mtp

# Disable VFR support for encoders
PRODUCT_PROPERTY_OVERRIDES += \
    debug.vfr.enable=0

# I/O Scheduler
PRODUCT_PROPERTY_OVERRIDES += \
    sys.io.scheduler=noop

# Low-RAM optimizations
ADDITIONAL_BUILD_PROPERTIES += \
    ro.config.low_ram=true \
    dalvik.vm.jit.codecachesize=0 \
    persist.sys.force_highendgfx=true \
    config.disable_atlas=true \
    dalvik.vm.dex2oat-flags=--no-watch-dog

# Use 4 threads max for dex2oat
ADDITIONAL_BUILD_PROPERTIES += \
    ro.sys.fw.dex2oat_thread_count=4

ADDITIONAL_BUILD_PROPERTIES += \
    ro.com.android.dataroaming=false

# Disabling strict mode
PRODUCT_PROPERTY_OVERRIDES += \
    persist.sys.strictmode.visual=0 \
    persist.sys.strictmode.disable=1

# RIL and modem settings
PRODUCT_PROPERTY_OVERRIDES += \
    rild.libpath=/system/lib/libxgold-ril.so \
    mobiledata.interfaces=rmnet0 \
    gsm.sim.num.pin2=3 \
    gsm.sim.num.puk2=10 \
    ro.config.endstksession=true \
    ro.config.sim_hot_swap=true \
    audioril.lib=libhuawei-audio-ril.so \
    modem.audio=1 \
    keyguard.no_require_sim=true \
    ro.ril.enable.amr.wideband=1 \
    telephony.lteOnCdmaDevice=0

# hsdpa+
PRODUCT_PROPERTY_OVERRIDES += \
    ro.config.hspap_hsdpa_open=1 \
    ro.ril.hsxpa=2 \
    ro.ril.gprsclass=10 \
    ro.ril.hsdpa.category=14

# Other phone network settings
PRODUCT_PROPERTY_OVERRIDES += \
    ro.telephony.call_ring.delay=1 \
    ring.delay=1 \
    telephony.lteOnGsmDevice=0 \
    ro.use_data_netmgrd=true \
    persist.data.netmgrd.qos.enable=false \
    ro.config.hw_gcf_mms = true

# Sensors settings
PRODUCT_PROPERTY_OVERRIDES += \
    ro.config.hw_proximitySensor=true \
    ro.config.hw_GSensorOptimize=true

# OpenGL settings
PRODUCT_PROPERTY_OVERRIDES += \
    ro.opengles.version=131072

# wlan settings
PRODUCT_PROPERTY_OVERRIDES += \
    wifi.interface=wlan0

# Fix deaf microphone
PRODUCT_PROPERTY_OVERRIDES += \
    ro.config.dualmic=true

# Fast mass storage
PRODUCT_PROPERTY_OVERRIDES += \
    ro.vold.umsdirtyratio=50

# OTG
PRODUCT_PROPERTY_OVERRIDES += \
    persist.sys.isUsbOtgEnabled=true

# Call recording
PRODUCT_PROPERTY_OVERRIDES += \
    persist.call_recording.enabled=1

# GPS
PRODUCT_PROPERTY_OVERRIDES += \
    ro.ril.def.agps.mode=2

# Enable force GPU rendering
# Fix color glitches
PRODUCT_PROPERTY_OVERRIDES += \
    persist.sys.ui.hw=true

# Set background Apps limit
PRODUCT_PROPERTY_OVERRIDES += \
    ro.sys.fw.bg_apps_limit=20

# Enable bservice
PRODUCT_PROPERTY_OVERRIDES += \
    ro.sys.fw.bservice_enable=true

# adb
ADDITIONAL_DEFAULT_PROPERTIES += \
    persist.service.adb.enable=1 \
    ro.debuggable=1 \
    ro.debug_level=0x4948

# Trim settings
PRODUCT_PROPERTY_OVERRIDES += \
    ro.sys.fw.use_trim_settings=true \
    ro.sys.fw.trim_empty_percent=80 \
    ro.sys.fw.trim_cache_percent=80 \

# CTS
PRODUCT_PROPERTY_OVERRIDES += \
    ro.keychain.hardware_backed=true

$(call inherit-product, hardware/broadcom/wlan/bcmdhd/firmware/bcm4330/device-bcm.mk)
