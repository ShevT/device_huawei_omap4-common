#!/bin/sh

dir=`cd $(dirname $0) && pwd`
top=$dir/../../../..

echo ""
echo "************************"
echo "*   CM based patches   *"
echo "************************"
for patch in `ls $dir/*.patch` ; do
    echo ""
    echo "==> patch file: ${patch##*/}"
    patch -p1 -N -i $patch -r - -d $top
done

echo "-== Copy Resources ==-"
cp -R -f -v $dir/frameworks .
cp -R -f -v $dir/packages .

cd $top
echo ""

rm -f packages/apps/Settings/res/drawable-hdpi/ic_cm_stats_notif.png
rm -f packages/apps/Settings/res/drawable-mdpi/ic_cm_stats_notif.png
rm -f packages/apps/Settings/res/drawable-xhdpi/ic_cm_stats_notif.png
